<?php
$num = $_POST['num'];

$arr1 = [
    0 => 1,
    1 => 1
];

for ($i = 2;$i < 100; $i++) {
    $arr1[$i] = $arr1[$i - 1] + $arr1[$i - 2];
};
$arr2 = array_search($num,$arr1);
if (!$arr2) {
    $date = [
        'status' => 0,
        'message' => $num.'不在斐波那契数里（100以内）',
        'date' => []
    ];
    echo json_encode($date,JSON_UNESCAPED_UNICODE);
    exit();
}
$date = [
    'status' => 0,
    'message' => '',
    'date' => [
        'isFibonacci' => true,
        'index' => $arr2 + 1,
    ]
];
echo json_encode($date,JSON_UNESCAPED_UNICODE);