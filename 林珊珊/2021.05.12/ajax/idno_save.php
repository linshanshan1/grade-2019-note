<?php
$num = $_GET['num'];

// 判断性别
$sex = substr($num,(strlen($num) == 18? -2:-1),1) % 2 ? '男':'女';

// 算年龄
$birth = substr($num,6,8);
$birthStr = strtotime($birth);

// 获取现在的时间戳
$day = time();

// 用现在的时间戳减去出生日期的时间戳
$date = floor(($day - $birthStr) / 86400 / 365);

// 判断生日是否过了，过了加一岁
$age = strtotime($birth.'+'.$date.'years') > $day ? ($date + 1) : $date;

echo '性别:'.$sex.'<br />';
echo '年龄:'.$age;