<?php
$words = explode(" ",$_GET['str']);

$wordList = [];

foreach ($words as $word) {
    if (empty($wordList[$word])) {
        $wordList[$word] = 0;
    }
    $wordList[$word] ++;
}
$date = [
    'status' => 1,
    'message' => "",
    'date' => [$wordList]
];

echo "词频:".json_encode($date);