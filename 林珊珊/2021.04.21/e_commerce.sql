CREATE DATABASE IF NOT EXISTS e_commerce
DEFAULT CHARACTER SET utf8mb4
DEFAULT COLLATE utf8mb4_general_ci;

use e_commerce;
-- 商品表
CREATE TABLE `goods` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '商品id',
  `name` varchar(45) NOT NULL COMMENT '商品名称',
  `cate_id` varchar(255) NOT NULL COMMENT '分类',
  `brand_id` int(11) NOT NULL DEFAULT '0' COMMENT '品牌',
  `price` int(11) NOT NULL DEFAULT '0' COMMENT '价格',
	`is_show` tinyint(3) NOT NULL DEFAULT '1' COMMENT '是否显示',
  `is_saleoff` tinyint(3) NOT NULL DEFAULT '0' COMMENT '是否售罄',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 comment='商品表';

-- 品牌表
CREATE TABLE `goods_brands` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '品牌id',
  `name` varchar(40) NOT NULL COMMENT '品牌名称',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
-- 分类表
CREATE TABLE `goods_cates` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '分类id',
  `name` varchar(40) NOT NULL COMMENT '分类名称',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 商品数据
insert into goods(id,name,cate_id,brand_id,price,is_show,is_saleoff)
	value(1,'r510vc 15.6英寸笔记本',4,1,3399,'1','0'),
	(2,'x550cc 15.6英寸笔记本',4,1,2799,'1','0'),
	(3,'x240 超极本',5,6,4880,'1','0'),
	(4,'u330p 13.3英寸超级本',5,6,4299,'1','0'),
	(5,'svp13226scb 触控超级本',6,5,7999,'1','0'),
	(6,'ipad mini 7.9英寸平板电脑',2,7,1999,'1','0'),
	(7,'iPad air 9.7英寸平板电脑',2,7,3388,'1','0'),
	(8,'iPad mini 配置 retine 显示屏',2,7,2788,'1','0'),
	(9,'ideacentre c3340 20英寸一体电脑',1,6,3499,'1','0'),
	(10,'vostro 3800-r1206 台式电脑',1,3,2899,'1','0'),
	(11,'15.6 寸电脑屏保护膜',3,4,29,'1','0'),
	(12,'优雅 复古 无线鼠标键盘',3,8,299,'1','0'),
	(13,'15寸 4K 液晶显示屏',3,5,1899,'1','0'),
	(14,'限量款 LOL 鼠标垫',3,2,29,'1','0');
-- 分类数据
insert into goods_cates(id,name)
	values(1,'台式机'),
	(2,'平板电脑'),
	(3,'电脑配件'),
	(4,'笔记本'),
	(5,'超极本'),
	(6,'超级本'),
	(8,'路由器'),
	(9,'交换机'),
	(10,'网卡');

-- 品牌数据
insert into goods_brands(id,name)
	values(1,'华硕'),
	(2,'唯爱'),
	(3,'戴尔'),
	(4,'爱戴尔'),
	(5,'索尼'),
	(6,'联想'),
	(7,'苹果'),
	(8,'雷蛇'),
	(16,'海尔'),
	(17,'清华同方'),
	(18,'神舟');
