<?php
declare (strict_types = 1);

namespace app\admin\controller;

use think\facade\View;

class Index
{
    public function index()
    {
        $count = 10;
        $arr = [1,2,3,4];
        View::assign('count',$count);
        View::assign('arr',$arr);
        return View::fetch();
    }
}
