<?php

namespace app\admin\controller;

use app\model\CategoryModel;
use think\facade\View;


Class Category
{

    public function index()
    {
        return View::fetch();
    }

    public function add()
    {
        return View::fetch();
    }

    public function addSave()
    {
        $category = CategoryModel::create([
            'category_name' => 'thinkphp',
            'category_desc' => 'thinkphp学习',
            'update_time' => time(),
            'add_time' => time(),
        ]);
        echo $category['category_id'] . "<br/>";
        echo $category['category_name'] . "<br/>";
        echo $category['category_desc'] . "<br/>";
        echo $category['update_time'] . "<br/>";
        echo $category['add_time'] . "<br/>";
    }

    public function edit()
    {
        //静态查询

        //1.查询category表里的全部数据
        //select * from category
        $category = CategoryModel::select();
        foreach ($category as $value) {
            echo $value . "<br/>";
        }
        echo "<br />";

        //2.查询id为1、2、3的分类
        //select * from category where category_id in (1,2,3);
        $category = CategoryModel::select([1, 2, 3]);
        foreach ($category as $value) {
            echo $value . "<br/>";
        }
        echo "<br />";

        //3.用降序查询category表里的全部数据
        //select * from category order by category_id desc
        $category = CategoryModel::order('category_id', 'desc')->select();
        foreach ($category as $value) {
            echo $value . "<br/>";
        }
        echo "<br />";

        //4.用降序查询category表里的id小于5
        //select * from category order by category_id desc
        $category = CategoryModel::where('category_id', '<', 5)
            ->order('category_id', 'desc')
            ->select();
        foreach ($category as $value) {
            echo $value . "<br/>";
        }
        echo "<br />";

        //5.查询id为1的分类
        $category = CategoryModel::where('category_id', 1)->find();
        echo $category['category_id'] . "<br/>";
        echo $category['category_name'] . "<br/>";
        echo $category['category_desc'] . "<br/>";
        echo $category['update_time'] . "<br/>";
        echo $category['add_time'] . "<br/>"."<br />";

        //6.查询category_name为thinkphp的分类
        $category = CategoryModel::where('category_name','thinkphp')->select();
        foreach ($category as $value) {
            echo $value . "<br/>";
        }
        echo "<br />";


        //7.查询前三条记录
        $category = CategoryModel::order('category_id','asc')
            ->limit(0,3)
            ->select();
        foreach ($category as $value){
            echo $value . "<br/>";
        }
        echo "<br />";


        //8.用count（）查询分类名称为thinkphp的总数
        //select count(*) as '总数' from category where category_name = 'thinkphp'
        $result = CategoryModel::where('category_name','thinkphp')
            -> count();
        echo "总数：".$result."<br/>"."<br />";

        //动态查询
        //1.查询分类id为1的分类
        $result = CategoryModel::getByCategoryId('1');
        echo "分类id：".$result['category_id'] . "<br/>";
        echo "分类名称：".$result['category_name'] . "<br/>";
        echo "分类描述：".$result['category_desc'] . "<br/>"."<br/>";


        //2.查询分类名称为mysql的分类
        $result = CategoryModel::getByCategoryName('sql server');
        echo "分类id：".$result['category_id'] . "<br/>";
        echo "分类名称：".$result['category_name'] . "<br/>";
        echo "分类描述：".$result['category_desc'] . "<br/>"."<br />";
    }
}