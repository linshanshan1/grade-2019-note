<?php

namespace app\admin\controller;


use app\model\BrandsModel;
use app\model\CatesModel;
use app\model\GoodsModel;

Class Goods
{
    public function goodsAdd()
    {
        //  1. 参考 82_电商数据 . md，系统增加了品牌、分类和商品，将其中相关数据插入到对应表
        $result = GoodsModel::create([
            ['name' => 'r510vc 15.6英寸笔记本', 'cate_id' => 4, 'brand_id' => 1, 'price' => 3399, 'is_show' => '1', 'is_saleoff' => '0'],
            ['name' => 'x550cc 15.6英寸笔记本', 'cate_id' => 4, 'brand_id' => 1, 'price' => 2799, 'is_show' => '1', 'is_saleoff' => '0'],
            ['name' => 'x240 超极本', 'cate_id' => 5, 'brand_id' => 6, 'price' => 4800, 'is_show' => '1', 'is_saleoff' => '0'],
            ['name' => 'u330p 13.3英寸超级本', 'cate_id' => 5, 'brand_id' => 6, 'price' => 4299, 'is_show' => '1', 'is_saleoff' => '0'],
            ['name' => 'svp13226scb 触控超级本', 'cate_id' => 6, 'brand_id' => 5, 'price' => 7999, 'is_show' => '1', 'is_saleoff' => '0'],
            ['name' => 'ipad mini 7.9英寸平板电脑', 'cate_id' => 2, 'brand_id' => 7, 'price' => 1999, 'is_show' => '1', 'is_saleoff' => '0'],
            ['name' => 'iPad air 9.7英寸平板电脑本', 'cate_id' => 2, 'brand_id' => 7, 'price' => 3388, 'is_show' => '1', 'is_saleoff' => '0'],
            ['name' => 'iPad mini 配置 retine 显示屏', 'cate_id' => 2, 'brand_id' => 7, 'price' => 2788, 'is_show' => '1', 'is_saleoff' => '0'],
            ['name' => 'ideacentre c3340 20英寸一体电脑', 'cate_id' => 1, 'brand_id' => 6, 'price' => 3499, 'is_show' => '1', 'is_saleoff' => '0'],
            ['name' => 'vostro 3800-r1206 台式电脑', 'cate_id' => 1, 'brand_id' => 3, 'price' => 2899, 'is_show' => '1', 'is_saleoff' => '0'],
            ['name' => '15.6 寸电脑屏保护膜', 'cate_id' => 4, 'brand_id' => 4, 'price' => 29, 'is_show' => '1', 'is_saleoff' => '0'],
            ['name' => '优雅 复古 无线鼠标键盘', 'cate_id' => 4, 'brand_id' => 8, 'price' => 299, 'is_show' => '1', 'is_saleoff' => '0'],
            ['name' => '15寸 4K 液晶显示屏', 'cate_id' => 4, 'brand_id' => 5, 'price' => 1899, 'is_show' => '1', 'is_saleoff' => '0'],
            ['name' => '限量款 LOL 鼠标垫', 'cate_id' => 4, 'brand_id' => 2, 'price' => 29, 'is_show' => '1', 'is_saleoff' => '0'],

        ]);
        echo $result . "<br/>";
    }

    public function cateAdd()
    {
        $result = CatesModel::create([
            ['name' => '华硕'],
            ['name' => '唯爱'],
            ['name' => '戴尔'],
            ['name' => '爱戴尔'],
            ['name' => '索尼'],
            ['name' => '联想'],
            ['name' => '苹果'],
            ['name' => '雷蛇'],
            ['name' => '海尔'],
            ['name' => '清华同方'],
            ['name' => '神舟']
        ]);
        echo $result . "<br/>";
    }

    public function brandsAdd()
    {
        $result = CatesModel::create([
            ['name' => '台式机'],
            ['name' => '平板电脑'],
            ['name' => '电脑配件'],
            ['name' => '笔记本'],
            ['name' => '超极本'],
            ['name' => '超极本'],
            ['name' => '路由器'],
            ['name' => '交换机'],
            ['name' => '网卡']
        ]);
        echo $result . "<br/>";
    }

    public function edit()
    {

        //	2. r510vc 15.6英寸笔记本 已经售罄，请修改相关数据。
        $goods = GoodsModel::update([
            'is_saleoff' => '1', 'id' => 1
        ]);
        echo $goods . "<br />";
        //	3. 15.6 寸电脑屏保护膜 下架处理，请修改相关数据。
        $goods = GoodsModel::update([
            'id' => 11,
            'name' => '15.6 寸电脑屏保护膜',
            'cate_id' => '3',
            'brand_id' => '4',
            'price' => '29',
            'is_show' => '0',
            'is_saleoff' => '0'
        ]);
        echo $goods . "<br/>";
        //	4. 查询所有的商品、品牌和分类数据。

        //商品表
        $goods = GoodsModel::select();
        foreach ($goods as $valus) {
            echo $valus . "<br />";
        }
        echo "<br/>";
        //分类表
        $cates = CatesModel::select();
        foreach ($cates as $valus) {
            echo $valus . "<br/>";
        }
        echo "<br/>";
        //品牌表
        $brands = BrandsModel::select();
        foreach ($brands as $valus) {
            echo $valus . "<br />";
        }
        echo "<br/>";

        //	5. 查询所有的商品，并且按照价格降序排列。

        $goods = GoodsModel::order('price', 'desc')->select();
        foreach ($goods as $valus) {
            echo $valus . "<br />";
        }
        echo "<br/>";

        //	6. 查询出品牌是戴尔的商品。

        $result = GoodsModel::Where('brand_id', '3')->select();
        foreach ($result as $valus) {
            echo $valus . "<br />";
        }
        echo "<br/>";

        //	7. 查询出所有超极本的商品。

        $result = GoodsModel::Where('cate_id', '5')->select();
        foreach ($result as $valus) {
            echo $valus . "<br />";
        }
        echo "<br/>";

        //	8. 查询华硕品牌的超极本。

        $result = GoodsModel::Where('cate_id = 5 and brand_id = 1')->select();
        foreach ($result as $valus) {
            echo $valus . "<br />";
        }
        echo "<br/>";

        //	9. 查询出价格最高的商品和最低的商品。

        //最高
        $result = GoodsModel::max('price');
        $result = GoodsModel::where('price', $result)->select();
        foreach ($result as $valus) {
            echo $valus . "<br />";
        }
        echo "<br/>";
        //最低
        $result = GoodsModel::min('price');
        $result = GoodsModel::where('price', $result)->select();
        foreach ($result as $valus) {
            echo $valus . "<br />";
        }
        echo "<br/>";

        //	10. 查询是否有售罄的商品。
        $result = GoodsModel::where('is_saleoff','1')->select();
        foreach ($result as $valus) {
            echo $valus . "<br />";
        }
        echo "<br/>";
        //	11. 查询所有商品、品牌和分类总数。

        $result = GoodsModel::count();
        echo "商品总数：".$result."<br />";

        $result = CatesModel::count();
        echo "分类总数：".$result."<br />";

        $result = BrandsModel::count();
        echo "品牌总数：".$result."<br />";

        //	12. 每页显示10件商品，查询出第1页和第2页的数据。

        $result = GoodsModel::limit('0','10')->order('id','asc')->select();
        echo "第一页". "<br />";
        foreach ($result as $valus) {
            echo $valus . "<br />";
        }
        echo "<br/>";

        $result = GoodsModel::limit('10','10')->order('id','asc')->select();
        echo "第二页". "<br />";
        foreach ($result as $valus) {
            echo $valus . "<br />";
        }
        echo "<br/>";
    }
}