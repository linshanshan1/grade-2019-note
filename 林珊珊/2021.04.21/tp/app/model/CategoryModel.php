<?php
namespace app\model;

use think\Model;

Class CategoryModel extends  Model {
    protected $name = 'category';   //数据库表名
    protected $pk = 'category_id';  //主键
}