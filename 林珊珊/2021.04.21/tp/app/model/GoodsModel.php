<?php
namespace app\model;

use think\Model;

Class GoodsModel extends Model {
    protected $name = 'goods';
    protected $pk = 'id';
}