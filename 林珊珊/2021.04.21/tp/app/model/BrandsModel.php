<?php
namespace app\model;

use think\Model;

Class BrandsModel extends Model{
    protected $name = 'goods_brands';
    protected $id = 'id';
}