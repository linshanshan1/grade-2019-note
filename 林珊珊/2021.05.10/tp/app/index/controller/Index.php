<?php
declare (strict_types = 1);

namespace app\index\controller;

use think\facade\Request;
use think\facade\Validate;

class Index
{
    public function index()
    {
        return '您好！这是一个[index]示例应用';
    }

    //1. 接第一章节练习题第2题，现在需要改进接口，移除首尾的空格再计算长度，请使用版本设计的方式改进。
    public function length()
    {
        $str = Request::param('str');
        $validate = Validate::rule([
            'str|内容' => 'require|min:1|max:255',
        ]);

        if (!$validate->check(['str' => $str])) {
            $date = [
                'status' => 0,
                'message' => $validate->getError(),
                'date' => []
            ];
            return json($date);
        }
        //移除首尾的空格再计算长度
        $words = mb_strlen(trim($str));

        $date = [
            'status' => 1,
            'message' => "",
            'date' => [$words]
        ];
        return json($date);
    }
    //接第一章节练习题第4题，现在需要改进接口，返回如下数据：
    public function word()
    {
        $v = Request::param('v',1);
        if($v==1){

            return $this->word1();

        }elseif ($v==2){

            return $this->word2();

        };

    }

    public function word1()
    {
        $content = Request::param('content');
        $validate = Validate::rule([
            'content|内容' => 'require|min:1|max:255',
        ]);

        if (!$validate->check(['content' => $content])) {
            $date = [
                'status' => 0,
                'message' => $validate->getError(),
                'date' => []
            ];
            return json($date);

        }
        $words = array_values(array_unique(explode(" ", $content)));

        $date = [
            'status' => 1,
            'message' => "",
            'date' => [$words]
        ];
        return json($date);
    }

    public function word2()
    {
        $content = Request::param('content');
        $validate = Validate::rule([
            'content|内容' => 'require|min:1|max:255',
        ]);

        if (!$validate->check(['content' => $content])) {
            $date = [
                'status' => 0,
                'message' => $validate->getError(),
                'date' => []
            ];
            return json($date);

        }
        $words = explode(" ", $content);

        $wordList = [];
        foreach ($words as $word) {
            if (empty($wordList[$word])) {
                $wordList[$word] = 0;
            }
            $wordList[$word]++;
        }
        $date = [
            'status' => 1,
            'message' => "",
            'date' => [$wordList]
        ];
        return json($date);
    }
}
