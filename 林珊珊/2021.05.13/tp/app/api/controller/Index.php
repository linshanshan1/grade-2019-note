<?php
declare (strict_types = 1);

namespace app\api\controller;

use think\facade\Request;
use think\facade\Validate;

class Index
{
    //设计一个api接口，判断数值是不是偶数
    public function is_oushu()
    {
        // 获取参数
        $num = Request::param('num');
        // 参数校验
        $validate = Validate::rule([
           'num|数值' => 'require|between:0,'.PHP_INT_MAX,
        ]);
        if(!$validate->check(['num'=>$num])){
            $date=[
                'status' => 1,
                'message' => $validate->getError(),
                'date' =>[]
            ];
            return json($date);
        }
        // 验证偶数逻辑
        $is_ouShu = $num % 2==0;
        // 输出json格式的数据
        $date=[
            'status' => 0,
            'message' => '',
            'date' =>[
                'is_ouShu'=>$is_ouShu
            ]
        ];
        return json($date);
    }
    //设计一个api接口，可以计算出传入字符的长度，并返回json格式。
    public function length()
    {
        $str = Request::param('str');

        $validate = Validate::rule([
            'str|数值' => 'require'
        ]);
        if(!$validate->check(['str'=>$str])){
            $date = [
              'status' => 1,
              'message' => $validate->getError(),
              'date'=>[]
            ];
            return json($date);
        }
        //验证字符长度
        $length = mb_strlen($str);

        $date = [
            'status' => 0,
            'message' => '',
            'data' => [
                'length' => $length,
            ],
        ];
        return json($date);
    }
    //设计一个api接口，可以计算出一个数字是否是斐波那契数列中的数字，并算出是第几位，并返回json格式。
    public function number()
    {
        $num = Request::param('num');

        $validate = Validate::rule([
            'num|数值' => 'require|between:0,'.PHP_INT_MAX
        ]);
        if(!$validate->check(['num'=>$num])){
            $date = [
                'status' => 1,
                'message' => $validate->getError(),
                'date'=>[]
            ];
            return json($date);
        }
        //判断斐波那契数
        //斐波那契数列：1 1 2 3 5 8 13 21 ...
        $arr = [
            0 =>1,
            1 =>1
        ];
        for($i = 2;$i<100;$i++){
            $arr[$i]=$arr[$i-1]+$arr[$i-2];
        }

		$addr = array_search($num,$arr);
        if($addr === false){
            $date = [
                'status' => 0,
                'message' => $num.'不在斐波那契数里（100以内）',
                'data' => []
            ];
            return json($date);

        }
        $date = [
            'status' => 0,
            'message' => '',
            'data' => [
                'isFibonacci' => true,
                'index' => $addr + 1,
            ],
        ];
        return json($date);
    }
    //设计一个api接口，可以分析一篇英文文章出现的单词列表，并返回json格式。
    public function word()
    {
        $content = Request::param("content");
        $validate = Validate::rule([
            'content|文章内容' => 'require|min:10|max:100',
        ]);
        if (!$validate->check(['content' => $content])) {
            $data = [
                'status' => 1,
                'message' => $validate->getError(),
                'data' => [],
            ];
            return json($data);
        }
        $words = array_values(array_unique(
            explode(" ", strtolower($content))
        ));

        $data = [
            'status' => 1,
            'message' => $validate->getError(),
            'data' => $words,
        ];
        return json($data);
    }

    public function id_card()
    {
        $num = Request::param("num");
        $validate = Validate::rule([
            'num|身份证' => 'require|min:10|max:100',
        ]);
        if (!$validate->check(['num' => $num])) {
            $data = [
                'status' => 1,
                'message' => $validate->getError(),
                'data' => [],
            ];
            return json($data);
        }
        //判断性别
        $sex = substr($num,(strlen($num)==18? -2:-1),1) % 2 ? '男':'女';

        //生日
        $birthday = substr($num,6,8);
        $birthdayStr = strtotime($birthday);
        //获取今日的时间戳
        $day = time();
        //用今天的时间戳减去生日的时间戳
        $diff = floor(($day - $birthdayStr) / 86400 / 365);
        //判断是否过生日过了就加一天
        $age = strtotime($birthday.'+'.$diff.'years') > $day ? ($diff + 1 ) : $diff;

        $data = [
            'status' => 1,
            'message' => $validate->getError(),
            'data' => [
                '性别：' => $sex,
                '年龄：' => $age
            ],
        ];
        return json($data);
    }
}
