CREATE DATABASE IF NOT EXISTS blog
DEFAULT CHARACTER SET utf8mb4
DEFAULT COLLATE utf8mb4_general_ci;

use blog;
CREATE TABLE `category` (
  `category_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '分类id',
  `category_name` varchar(45) NOT NULL COMMENT '分类名称',
  `category_desc` varchar(255) NOT NULL COMMENT '分类描述',
  `update_time` int(11) NOT NULL DEFAULT '0' COMMENT '记录更新时间',
  `add_time` int(11) NOT NULL COMMENT '记录增加时间',
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 comment='分类表';

CREATE TABLE `article` (
  `article_id` int(11) unsigned AUTO_INCREMENT NOT NULL COMMENT '文章id',
  `category_id` int(11) NOT NULL COMMENT '所属分类',
  `article_title` varchar(45) NOT NULL COMMENT '标题',
	`article_desc` varchar(500) not null comment '文章简介',
  `content` longtext NOT NULL COMMENT '文章内容',
  `update_time` int(11) NOT NULL COMMENT '更新时间',
  `add_time` int(11) NOT NULL COMMENT '增加时间',
  PRIMARY KEY (`article_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 comment='文章表';

CREATE TABLE `admin`(
  `admin_id` int(11) unsigned AUTO_INCREMENT NOT NULL COMMENT '用户id',
  `admin_name` varchar(10) NOT NULL COMMENT '用户名称',
  `admin_email` varchar(100)  NOT NULL COMMENT '用户邮箱',
  `admin_pwd` varchar(45) NOT NULL COMMENT '用户密码',
  `update_time` int(11) NOT NULL COMMENT '更新时间',
  `add_time` int(11) NOT NULL COMMENT '增加时间',
   PRIMARY KEY (`admin_id`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 comment='用户表';

-- 分类信息
insert into category(category_name,category_desc,update_time,add_time)
	value('mysql','mysql基础知识',1616479958,1616479958),
			('sql server','sql server基础知识',1616479958,1616479958);
-- 文章信息
insert into article(category_id,article_title,article_desc,content,update_time,add_time)
	value(1,'mysql','mysql简介','mysql内容',1616479958,1616479958),
			(2,'sql server','sql server简介','sql server内容',1616479958,1616479958);
-- 用户信息
insert into admin(admin_name,admin_email,admin_pwd,update_time,add_time)
	value('admin','1250205160@qq.com','123456',1616479958,1616479958),
			('air','1234567890@qq.com','321654',1616479958,1616479958);
