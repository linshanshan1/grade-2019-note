//全选
$("#all").click(function () {
    $(".all_btn").prop("checked",true);
});

// 批量删除
$(".all_delete").click(function () {
    if (window.confirm("是否批量删除？")) {
        $("#form_delete").submit();
    }
});

// 管理员批量删除
$("#admin_delete").click(function () {
    window.confirm("您没有权限删除管理员！");
});

