<?php
/**
 * 注册成功
 */
date_default_timezone_set("PRC");

$addTime = time();
$updateTime = $addTime;

$dsn = "mysql:host=127.0.0.1;dbname=blog";
$db = new PDO($dsn,'root','123456');
$db ->exec("set name utf8mb4");

if ($_POST['admin_pwd'] != $_POST['pwd_again']) {
    echo "两次密码不一致，请重新输入<br />";
    echo '<a href="javascript:void(0)" onclick="history.back();">返回上一页</a>';
    exit();
}

$sql = "insert into admin(admin_name,admin_email,admin_pwd,update_time,add_time)
          value('".$_POST['admin_name']."','".$_POST['admin_email']."','".$_POST['admin_pwd']."','$updateTime','$addTime');";
$result =$db->exec($sql);

if($result){
    $log = array (
        "用户名" => $_POST['admin_name'],
        "邮箱" => $_POST['admin_email'],
        'ip' => $_SERVER['REMOTE_ADDR'],
        'action' => 'register',
        '状态' => '注册成功',
        '时间' => date("Y-m-d H:i:s", time()),
    );
    $logArr = json_encode($log,JSON_UNESCAPED_UNICODE);
    file_put_contents("../log/log.txt",$logArr.PHP_EOL,FILE_APPEND);

    echo "注册成功<br />";
    echo "<a href='login.php'>返回登录页面</a>";
}else{
    $log = array (
        "用户名" => $_POST['admin_name'],
        "邮箱" => $_POST['admin_email'],
        'ip' => $_SERVER['REMOTE_ADDR'],
        'action' => 'register',
        '状态' => '注册失败',
        '时间' => date("Y-m-d H:i:s", time()),
    );
    $logArr = json_encode($log,JSON_UNESCAPED_UNICODE);
    file_put_contents("../log/log.txt",$logArr.PHP_EOL,FILE_APPEND);

    echo "注册失败，错误信息：".$db->errorInfo()[2].",请联系管理员：1250205160@qq.com";
}