<?php
/**
 * 管理员列表
 */
session_start();
date_default_timezone_set("PRC");

$dsn = "mysql:host=127.0.0.1;dbname=blog";
$db = new PDO($dsn,"root","123456");
$db->exec("set name utf8mb4");

$sql = "select * from admin order by admin_id asc;";
$result = $db->query($sql);
$admin = $result->fetchAll(PDO::FETCH_ASSOC);

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>管理员列表</title>
    <link rel="stylesheet" href="/blog/css/blog.css" type="text/css">
    <script src="/blog/js/jquery.js"></script>
</head>
<body>
<div id="blog-container">
    <div id="blog-top-left">
        <div id="blog-left-title">
            <h2>博客管理系统</h2>
        </div>
    </div>
    <div id="blog-top-right">
        <div id="blog-right-title">
            <h4>欢迎你：<?php echo $_SESSION['adminName']?> <a href="logout.php">退出登录</a></h4>
        </div>
    </div>
    <div id="blog-left">
        <ul>
            <li><a href="category_list.php">分类管理</a></li>
            <li><a href="article_list.php">文章管理</a></li>
            <li><a href="admin_list.php">管理员</a></li>
        </ul>
    </div>
    <div id="blog-right">
        <p><a href="#">首页</a> > <a href="category_list.php">分类列表</a> > <a href="article_list.php">文章列表</a> > <a href="admin_list.php">管理员列表</a></p>
        <p>
            <input type="button" value="全选" id="all">
            <a href="#" id="admin_delete"> 删除选中任务</a>
            <span><a href="admin_add.php">增加管理员</a></span>
        </p>
        <form method="post" class="admin_form">
            <table>
                <tr>
                    <th></th>
                    <th>管理员ID</th>
                    <th>用户名</th>
                    <th>邮箱</th>
                    <th>创建时间</th>
                    <th>修改时间</th>
                    <th>操作</th>
                </tr>
                <?php foreach($admin as $value):?>
                    <tr>
                        <td><input type="checkbox" class="all_btn" name="admin_id[]" value="<?php echo $value['admin_id']?>"></td>
                        <td><?php echo $value['admin_id']; ?></td>
                        <td><?php echo $value['admin_name']; ?></td>
                        <td><?php echo $value['admin_email']; ?></td>
                        <td><?php echo date('Y-m-d H:i:s',$value['add_time']); ?></td>
                        <td><?php echo date('Y-m-d H:i:s',$value['update_time']); ?></td>
                        <td>
                            <a href="admin_edit.php?id=<?php echo $value['admin_id']; ?>">编辑</a>
<!--                            <a href="admin_detele.php?id=--><?php //echo $value['admin_id']; ?><!--">删除</a>-->
                        </td>
                    </tr>
                <?php endforeach;?>
            </table>
        </form>
    </div>
</div>
<script src="/blog/js/main.js"></script>
</body>
</html>
