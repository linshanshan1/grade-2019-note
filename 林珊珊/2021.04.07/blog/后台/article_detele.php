<?php
/**
 * 文章删除
 */
date_default_timezone_set("PRC");

$dsn = "mysql:host=127.0.0.1;dbname=blog";
$db = new PDO($dsn,"root","123456");
$db->exec("set name utf8mb4");

$sql = "delete from article where article_id = '".$_GET['id']."';";
$result = $db->exec($sql);
if($result){
    $log = array (
        "article_id" => $_GET['id'],
        'ip' => $_SERVER['REMOTE_ADDR'],
        'action' => 'delete',
        '状态' => '删除成功',
        '时间' => date("Y-m-d H:i:s", time()),
    );
    $logArr = json_encode($log,JSON_UNESCAPED_UNICODE);
    file_put_contents("../log/log.txt",$logArr.PHP_EOL,FILE_APPEND);

    echo "删除成功<br />";
    echo "<a href='article_list.php'>返回文章列表</a>";
}else{
    $log = array (
        "article_id" => $_GET['id'],
        'ip' => $_SERVER['REMOTE_ADDR'],
        'action' => 'delete',
        '状态' => '删除失败',
        '时间' => date("Y-m-d H:i:s", time()),
    );
    $logArr = json_encode($log,JSON_UNESCAPED_UNICODE);
    file_put_contents("../log/log.txt",$logArr.PHP_EOL,FILE_APPEND);

    echo "删除失败，错误信息：".$db->errorInfo()[2].",请联系管理员：1250205160@qq.com";
}
?>