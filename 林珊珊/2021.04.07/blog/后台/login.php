<?php
/**
 * 登录页面
 */
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>登录页面</title>
    <link rel="stylesheet" href="/blog/css/admin.css" type="text/css" />
</head>
<body>
<div id="container">
    <h2>非常日记后台管理</h2>
    <div id="middle">
        <p>请输入邮箱和密码</p>
        <form action="login_save.php" method="post">
            <input type="text" name="user_email" class="text" placeholder="邮箱"/>
            <input type="text" name="user_pwd" class="text" placeholder="密码"/>
            <input type="text" name="check" class="check" placeholder="验证码" />
            <img src="image.php" class="image"/>
            <a href="login.php" class="change" style="color: #2E5F9A;font-size: 10px;text-decoration: #2E5F9A underline;margin: 0px 0px 0px 195px">看不清，换一张</a>
            <input type="checkbox" name="rememberMe" class="rememberMe" /> 记住我
            <input type="submit" value="登录" id="button"/>
        </form>
    </div>
    <a href="register.php">新管理员？创建账号</a>
    <p style="margin: 60px 0 0 400px;color: white;">
        访问了
        <?php
        session_start();
        if (empty($_SESSION['count'])) {
            $_SESSION['count'] = 1;
        } else {
            $_SESSION['count'] = $_SESSION['count'] + 1;
        }
        echo $_SESSION['count'];
        ?>
        次
    </p>
</div>
</body>
</html>



