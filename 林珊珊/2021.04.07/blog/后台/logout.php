<?php
/**
 * 退出登录
 */
session_start();

unset($_SESSION['adminId']);
unset($_SESSION['adminName']);
unset($_SESSION['adminEmail']);

$log = array (
//    "用户id" => $_SESSION['adminId'],
//    "用户名" => $_SESSION['adminName'],
//    "邮箱" => $_SESSION['adminEmail'],
    'ip' => $_SERVER['REMOTE_ADDR'],
    'action' => 'logout',
    '状态' => '退出登录成功',
    '时间' => date("Y-m-d H:i:s", time()),
);
$logArr = json_encode($log,JSON_UNESCAPED_UNICODE);
file_put_contents("../log/log.txt",$logArr.PHP_EOL,FILE_APPEND);

echo "退出登录成功!<br />";
echo "<a href='login.php'>返回登录界面</a>";
exit();


