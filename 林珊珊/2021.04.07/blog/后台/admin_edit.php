<?php
/**
 * 管理员编辑
 */
session_start();
date_default_timezone_set("PRC");

$updateTime = time();

$dsn = "mysql:host=127.0.0.1;dbname=blog";
$db = new PDO($dsn,'root','123456');
$db ->exec("set name utf8mb4");

$sql = "select * from admin where admin_id = '".$_GET['id']."';";
$result = $db ->query($sql);
$admin = $result -> fetch(PDO::FETCH_ASSOC);
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>管理员编辑</title>
    <link rel="stylesheet" href="/blog/css/blog.css" type="text/css">
</head>
<body>
<div id="blog-container">
    <div id="blog-top-left">
        <div id="blog-left-title">
            <h2>博客管理系统</h2>
        </div>
    </div>
    <div id="blog-top-right">
        <div id="blog-right-title">
            <h4>欢迎你：<?php echo $_SESSION['adminName']?> <a href="logout.php">退出登录</a></h4>
        </div>
    </div>
    <div id="blog-left">
        <ul>
            <li><a href="category_list.php">分类管理</a></li>
            <li><a href="article_list.php">文章管理</a></li>
            <li><a href="admin_list.php">管理员</a></li>
        </ul>
    </div>
    <div id="blog_add-right">
        <p><a href="#">首页</a> > <a href="category_list.php">分类列表</a> > <a href="article_list.php">文章列表</a> > <a href="admin_list.php">管理员列表</a></p>
        <form action="admin_edit_save.php" method="post">
            <table>
                <tr>
                    <td class="title">管理员id:</td>
                    <td class="b">
                        <input type="text" name="admin_id" value="<?php echo $admin['admin_id']; ?>" readonly="readonly">
                    </td>
                </tr>
                <tr>
                    <td class="title">用户名:</td>
                    <td class="b"><input type="text" name="admin_name" value="<?php echo $admin['admin_name']; ?>"></td>
                </tr>
                <tr>
                    <td class="title">邮箱:</td>
                    <td class="b"><input type="text" name="admin_email" value="<?php echo $admin['admin_email']; ?>" readonly="readonly"></td>
                </tr>
                <tr>
                    <td class="title">密码:</td>
                    <td class="b"><input type="text" name="admin_pwd" value="<?php echo $admin['admin_pwd']; ?>"></td>
                </tr>
                <tr>
                    <td></td>
                    <td class="b">
                        <input type="submit" name="submit" class="btn">
                        <input type="reset" name="reset" class="btn">
                    </td>
                </tr>
            </table>
        </form>
    </div>
</div>
</body>
</html>

