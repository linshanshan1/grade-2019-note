<?php
/**
 * 登录完成
 */
$rememberMe = $_POST['rememberMe'] ?? " ";

$dsn = "mysql:host=127.0.0.1;dbname=blog";
$db = new PDO($dsn,"root","123456");

$sql = "select * from admin where admin_email = '{$_POST['user_email']}'";
$result = $db->query($sql);
$admin = $result->fetch(PDO::FETCH_ASSOC);

//判断用户是否存在    判断用户名称，用户密码是否正确
if(($admin) && ($admin['admin_pwd'] == $_POST['user_pwd'])){
    session_start();
    $_SESSION["adminEmail"] = $admin['admin_email'];
    $_SESSION["adminName"]= $admin['admin_name'];

    // 判断验证码是否正确
    if ($_POST['check'] == $_SESSION['check']) {
        "验证码正确";
    }elseif (empty($_POST['check'])) {
        echo "验证码不能为空";
        echo "<a href='javascript:void(0)' onclick='history.back()'>返回上一页</a>";
        exit();
    }else {
        echo "1:" .$_POST['check'];
        echo "2:" .$_SESSION['check'];
        echo "验证码错误";
        echo "<a href='javascript:void(0)' onclick='history.back()'>返回上一页</a>";
        exit();
    }

    // 记住我一天
    if ( $rememberMe) {
        setcookie('PHPSESSID',session_id(),time()+86400);
    }

    $log = array (
        "用户名" => $_SESSION['adminName'],
        "邮箱" => $_SESSION['adminEmail'],
        'ip' => $_SERVER['REMOTE_ADDR'],
        'action' => 'login',
        '状态' => '登录后台成功',
        '时间' => date("Y-m-d H:i:s", time()),
    );
    $logArr = json_encode($log,JSON_UNESCAPED_UNICODE);
    file_put_contents("../log/log.txt",$logArr.PHP_EOL,FILE_APPEND);

    echo '登录成功<br/>';
    echo "<a href='article_list.php'>进入文章列表</a>";
}else{
    $log = array (
        "用户名" => $_SESSION['adminName'],
        "邮箱" => $_SESSION['adminEmail'],
        'ip' => $_SERVER['REMOTE_ADDR'],
        'action' => 'login',
        '状态' => '登录后台失败',
        '时间' => date("Y-m-d H:i:s", time()),
    );
    $logArr = json_encode($log,JSON_UNESCAPED_UNICODE);
    file_put_contents("../log/log.txt",$logArr.PHP_EOL,FILE_APPEND);

    echo "登录失败,账号或者密码错误！<br/>";
    echo "<a href='javascript:void(0)' onclick='history.back();'>返回上一页</a>";
}