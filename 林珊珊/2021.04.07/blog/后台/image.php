<?php
session_start();

$image =imagecreate(100,30);
$color = imagecolorallocate($image,113,178,218); // 背景色

$check = '';


for ($i = 1;$i <= 4;$i++) { // 生成6位随机字符
    // 随机生成6位验证码（字母和数字）
    $str = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'; // 除了0-9数字，还可以支持a-z、A-Z的字符
    $font = substr($str,mt_rand(0,mb_strlen($str)),1);
    $check .= $font;

    $fontColor = imagecolorallocate($image,mt_rand(0,120),mt_rand(0,120),mt_rand(0,120)); // 生成随机颜色
    imagestring($image,5,20 * $i,8,$font,$fontColor);
}

for ($i = 1;$i <= 6;$i++) { // 生成6条随机横线
    $lineColor = imagecolorallocate($image,mt_rand(50,200),mt_rand(50,200),mt_rand(50,200)); // 生成随机颜色
    imageline($image,mt_rand(1,99),mt_rand(1,29),mt_rand(1,99),mt_rand(1,29),$lineColor);
}

for ($i = 1;$i <= 30;$i++) { // 生成随机雪花
    $pointColor = imagecolorallocate($image,mt_rand(0,225),mt_rand(0,225),mt_rand(0,225)); // 生成随机颜色
    imagesetpixel($image,mt_rand(1,29),mt_rand(1,99),$pointColor);
}
$_SESSION['check'] = $check;

header("Content-Type: image/png");
imagepng($image); // 生成图片
imagedestroy($image); // 销毁一图像，释放资源