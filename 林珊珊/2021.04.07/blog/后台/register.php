<?php
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>注册页面</title>
    <link rel="stylesheet" href="/blog/css/admin.css" type="text/css">
</head>
<body>
<div id="container">
    <h2>非常日记管理员注册</h2>
    <div id="middle">
        <p>请输入相关信息</p>
        <form action="register_save.php" method="post">
            <input type="text" name="admin_name" class="text" placeholder="用户名"/>
            <input type="text" name="admin_email" class="text" placeholder="邮箱"/>
            <input type="text" name="admin_pwd" class="text" placeholder="密码"/>
            <input type="text" name="pwd_again" class="text" placeholder="确认密码"/>
            <input type="submit" value="注册" id="button"/>
        </form>
    </div>
    <a href="login.php">已有账号？ 登录</a>
</div>

</body>
</html>
