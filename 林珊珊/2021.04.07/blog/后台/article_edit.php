<?php
/**
 * 文章编辑
 */
session_start();
date_default_timezone_set("PRC");

$dsn = "mysql:host=127.0.0.1;dbname=blog";
$db = new PDO($dsn,'root','123456');
$db ->exec("set name utf8mb4");
//文章信息
$sql = "select * from article where article_id = '".$_GET['id']."'; ";
$result =$db->query($sql);
$article = $result->fetch(PDO::FETCH_ASSOC);
//分类信息
$sql = "select * from category ORDER BY category_id ASC; ";
$result =$db->query($sql);
$category = $result->fetchAll(PDO::FETCH_ASSOC);
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>文章编辑</title>
    <link rel="stylesheet" href="/blog/css/blog.css" type="text/css">
</head>
<body>
<div id="blog-container">
    <div id="blog-top-left">
        <div id="blog-left-title">
            <h2>博客管理系统</h2>
        </div>
    </div>
    <div id="blog-top-right">
        <div id="blog-right-title">
            <h4>欢迎你：<?php echo $_SESSION['adminName']?> <a href="logout.php">退出登录</a></h4>
        </div>
    </div>
    <div id="blog-left">
        <ul>
            <li><a href="category_list.php">分类管理</a></li>
            <li><a href="article_list.php">文章管理</a></li>
            <li><a href="admin_list.php">管理员</a></li>
        </ul>
    </div>
    <div id="blog_add-right">
        <p><a href="#">首页</a> > <a href="category_list.php">分类列表</a> > <a href="article_list.php">文章列表</a> > <a href="admin_list.php">管理员列表</a></p>
        <form action="article_edit_save.php" method="post">
            <table>
                <tr>
                    <td class="title">文章id:</td>
                    <td class="b">
                        <input type="text" name="article_id" value="<?php echo $article['article_id']; ?>" readonly="readonly">
                    </td>
                </tr>
                <tr>
                    <td class="title">文章标题:</td>
                    <td class="b"><input type="text" name="article_title" value="<?php echo $article['article_title']; ?>"></td>
                </tr>
                <tr>
                    <td class="title">所属分类:</td>
                    <td class="b">
                        <select id="content" name="category_id">
                            <?php foreach ($category as $value):?>
                                <option value="<?php echo $value['category_id']?>"<?php $article['article_id'] == $value['category_id']? 'selected="selected"':'';?>>
                                    <?php echo $value['category_name']?>
                                </option>
                            <?php endforeach;?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td class="title">文章简介:</td>
                    <td class="b">
                        <textarea style="width: 350px;height: 200px;" name="article_desc"><?php echo $article['article_desc']?></textarea>
                    </td>
                </tr>
                <tr>
                    <td class="title">文章内容：</td>
                    <td class="b">
                        <textarea style="width: 350px;height: 200px;" name="content"><?php echo $article['content']?></textarea>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td class="b">
                        <input type="submit" name="submit" class="btn">
                        <input type="reset" name="reset" class="btn">
                    </td>
                </tr>
            </table>
        </form>
    </div>
</div>
</body>
</html>

