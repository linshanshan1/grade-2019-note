<?php
/**
 * 编辑成功
 */
date_default_timezone_set("PRC");

$updateTime = time();

$dsn = "mysql:host=127.0.0.1;dbname=blog";
$db = new PDO($dsn,'root','123456');
$db ->exec("set name utf8mb4");

//文章标题限制1-10个字
if(mb_strlen($_POST['article_title']) < 1 || mb_strlen($_POST['article_title']) > 10){
    $log = array (
        "article_title" => $_POST['article_title'],
        'ip' => $_SERVER['REMOTE_ADDR'],
        'action' => 'article_edit',
        '状态' => '文章标题限制1-10个字',
        '时间' => date("Y-m-d H:i:s", time()),
    );
    $logArr = json_encode($log,JSON_UNESCAPED_UNICODE);
    file_put_contents("../log/log.txt",$logArr.PHP_EOL,FILE_APPEND);

    echo '文章标题限制1-10个字';
    echo '<a href="javascript:void(0)" onclick="history.back();">返回上一页</a>';
    exit();
}
//所属分类未选择
if(empty($_POST['category_id'])){
    $log = array (
        "category_id" => $_POST['category_id'],
        'ip' => $_SERVER['REMOTE_ADDR'],
        'action' => 'article_edit',
        '状态' => '文章的所属分类未选择',
        '时间' => date("Y-m-d H:i:s", time()),
    );
    $logArr = json_encode($log,JSON_UNESCAPED_UNICODE);
    file_put_contents("../log/log.txt",$logArr.PHP_EOL,FILE_APPEND);

    echo '文章的所属分类未选择';
    echo '<a href="javascript:void(0)" onclick="history.back();">返回上一页</a>';
    exit();
}
//文章简介限制10-50个字
if(mb_strlen($_POST['article_desc']) < 10 || mb_strlen($_POST['article_desc']) > 50){
    $log = array (
        "article_desc" => $_POST['article_desc'],
        'ip' => $_SERVER['REMOTE_ADDR'],
        'action' => 'article_edit',
        '状态' => '文章简介限制10-50个字',
        '时间' => date("Y-m-d H:i:s", time()),
    );
    $logArr = json_encode($log,JSON_UNESCAPED_UNICODE);
    file_put_contents("../log/log.txt",$logArr.PHP_EOL,FILE_APPEND);

    echo '文章简介限制10-50个字';
    echo '<a href="javascript:void(0)" onclick="history.back();">返回上一页</a>';
    exit();
}
//文章内容10-100个字
if(mb_strlen($_POST['content']) < 10 || mb_strlen($_POST['content']) > 100){
    $log = array (
        "content" => $_POST['content'],
        'ip' => $_SERVER['REMOTE_ADDR'],
        'action' => 'article_edit',
        '状态' => '文章内容限制10-50个字',
        '时间' => date("Y-m-d H:i:s", time()),
    );
    $logArr = json_encode($log,JSON_UNESCAPED_UNICODE);
    file_put_contents("../log/log.txt",$logArr.PHP_EOL,FILE_APPEND);

    echo '文章内容限制10-50个字';
    echo '<a href="javascript:void(0)" onclick="history.back();">返回上一页</a>';
    exit();
}

$sql = "update article set category_id='".$_POST['category_id']."',
                        article_title='".$_POST['article_title']."',
                        article_desc='".$_POST['article_desc']."',
                        content='".$_POST['content']."',
                        update_time='$updateTime'
                            where article_id = '".$_POST['article_id']."';";
$result =$db->exec($sql);

if($result){
    $log = array (
        "category_id" => $_POST['category_id'],
        "article_title" => $_POST['article_title'],
        "article_desc" => $_POST['article_desc'],
        "content" => $_POST['content'],
        "article_id" => $_POST['article_id'],
        'ip' => $_SERVER['REMOTE_ADDR'],
        'action' => 'article_edit',
        '状态' => '文章编辑成功',
        '时间' => date("Y-m-d H:i:s", time()),
    );
    $logArr = json_encode($log,JSON_UNESCAPED_UNICODE);
    file_put_contents("../log/log.txt",$logArr.PHP_EOL,FILE_APPEND);

    echo "文章编辑成功<br />";
    echo "<a href='article_list.php'>返回文章列表</a>";
}else{
    $log = array (
        "category_id" => $_POST['category_id'],
        "article_title" => $_POST['article_title'],
        "article_desc" => $_POST['article_desc'],
        "content" => $_POST['content'],
        "article_id" => $_POST['article_id'],
        'ip' => $_SERVER['REMOTE_ADDR'],
        'action' => 'article_edit',
        '状态' => '文章编辑失败',
        '时间' => date("Y-m-d H:i:s", time()),
    );
    $logArr = json_encode($log,JSON_UNESCAPED_UNICODE);
    file_put_contents("../log/log.txt",$logArr.PHP_EOL,FILE_APPEND);

    echo "文章编辑失败，错误信息：".$db->errorInfo()[2].",请联系管理员：1250205160@qq.com";
}
?>