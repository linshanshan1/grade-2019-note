<?php
/**
 * 文章列表
 */
$dsn = mysqli_connect('localhost','root','xyz123456');
$sql = "select * from article order by article_id desc";
$result = mysqli_query($dsn,$sql);
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>博客管理系统</title>
    <link rel="stylesheet" href="/博客/css/blog.css" type="text/css">
</head>
<body>
<div id="blog-container">
    <div id="blog-top-left">
        <div id="blog-left-title">
            <h2>博客管理系统</h2>
        </div>
    </div>
    <div id="blog-top-right">
        <div id="blog-right-title">
            <h4>欢迎你：admin <a href="#">退出登录</a></h4>
        </div>
    </div>
    <div id="blog-left">
        <ul>
            <li><a href="#">分类管理</a></li>
            <li><a href="#">新闻管理</a></li>
            <li><a href="#">管理员</a></li>
        </ul>
    </div>
    <div id="blog-right">
        <p><a href="#">首页</a> > <a href="#">新闻管理</a> > <a href="#">新闻列表</a></p>
        <p>
            <input type="button" value="全选" id="all">
            <a href="#"> 删除选中任务</a>
            <span><a href="#">增加新闻</a></span>
        </p>
        <table>
            <tr>
                <td></td>
                <th>ID</th>
                <th>标题</th>
                <th>分类</th>
                <th>简介</th>
                <th>发表时间</th>
                <th>修改时间</th>
                <th>操作</th>
            </tr>
            <?php foreach($result as $value):?>
            <tr>
                <td><input type="checkbox"></td>
                <td><?php echo $value['article_id']?></td>
                <td><?php echo $value['article_title']?></td>
                <td><?php
                    $sql = "select * from category where category_id = '{$value['category_id']}';"
                    ?>
                </td>
                <td><?php echo $value['content']?></td>
                <td><?php echo $value['update_time']?></td>
                <td><?php echo $value['add_time']?></td>
                <td><a href="#">编辑</a></td>
            </tr>
            <?php endforeach;?>
        </table>

    </div>
</div>
</body>
</html>