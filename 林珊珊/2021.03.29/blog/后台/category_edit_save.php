<?php
/**
 * 分类编辑完成
 */
date_default_timezone_set("PRC");

$updateTime = time();

$dsn = "mysql:host=127.0.0.1;dbname=blog";
$db = new PDO($dsn,'root','123456');
$db ->exec("set name utf8mb4");

///分类名称1-10
if(mb_strlen($_POST['category_name']) < 1 || mb_strlen($_POST['category_name']) > 10 ){
    echo '分类名称限制5-10个字';
    echo '<a href="javascript:void(0)" onclick="history.back();">返回上一页</a>';
    exit();
};
//分类描述10-255
if(mb_strlen($_POST['category_desc']) < 10 || mb_strlen($_POST['category_desc']) > 255){
    echo '分类描述限制10-255个字';
    echo '<a href="javascript:void(0)" onclick="history.back();">返回上一页</a>';
    exit();
};

$sql = "update category set category_name = '".$_POST['category_name']."',
            category_desc = '".$_POST['category_desc']."',
            update_time = '$updateTime'
                where category_id = '".$_POST['id']."';";
$result =$db->exec($sql);

if($result){
    echo "编辑成功<br />";
    echo "<a href='category_list.php'>返回分类列表</a>";
}else{
    echo "编辑失败，错误信息：".$db->errorInfo()[2].",请联系管理员：1250205160@qq.com";
}
?>