<?php
/**
 * 文章增加完成
 */
date_default_timezone_set("PRC");

$addTime = time();
$updateTime = $addTime;

$dsn = "mysql:host=127.0.0.1;dbname=blog";
$db = new PDO($dsn,'root','123456');
$db ->exec("set name utf8mb4");

//文章标题限制1-10个字
if(mb_strlen($_POST['article_title']) < 1 || mb_strlen($_POST['article_title']) > 10){
    echo '文章标题限制1-10个字';
    echo '<a href="javascript:void(0)" onclick="history.back();">返回上一页</a>';
    exit();
}
//所属分类未选择
if(empty($_POST['category_id'])){
    echo '文章的所属分类未选择';
    echo '<a href="javascript:void(0)" onclick="history.back();">返回上一页</a>';
    exit();
}
//文章简介限制10-50个字
if(mb_strlen($_POST['article_desc']) < 10 || mb_strlen($_POST['article_desc']) > 50){
    echo '文章简介限制10-50个字';
    echo '<a href="javascript:void(0)" onclick="history.back();">返回上一页</a>';
    exit();
}
//文章内容20-100个字
if(mb_strlen($_POST['content']) < 20 || mb_strlen($_POST['content']) > 100){
    echo '文章内容限制10-50个字';
    echo '<a href="javascript:void(0)" onclick="history.back();">返回上一页</a>';
    exit();
}

$sql = "insert into article(category_id,article_title,article_desc,content,update_time,add_time)
          value('".$_POST['category_id']."','".$_POST['article_title']."','".$_POST['article_desc']."','".$_POST['content']."','$updateTime','$addTime');";
$result =$db->exec($sql);

if($result){
    echo "增加成功<br />";
    echo "<a href='article_list.php'>返回主页面</a>";
}else{
    echo "增加失败，错误信息：".$db->errorInfo()[2].",请联系管理员：1250205160@qq.com";
}
?>