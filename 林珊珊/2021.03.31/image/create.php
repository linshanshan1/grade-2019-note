<?php
$image =imagecreate(100,30);
$color = imagecolorallocate($image,113,178,218); // 背景色


// 随机生成6位验证码（字母和数字）
$str = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'; // 除了0-9数字，还可以支持a-z、A-Z的字符
$max = mb_strlen($str) - 1;

$strLen = mb_strlen($str);
for ($i = 1;$i <= 6;$i++) { // 生成6位随机字符
    $fontColor = imagecolorallocate($image,mt_rand(1,120),mt_rand(1,120),mt_rand(1,120),); // 生成随机颜色
    $n = mt_rand(0,$max);
    imagestring($image,5,13 * $i,8,$str[$n],$fontColor);
}

for ($i = 1;$i <= 6;$i++) {
    $lineColor = imagecolorallocate($image,mt_rand(50,200),mt_rand(50,200),mt_rand(50,200),); // 生成随机颜色
    imageline($image,mt_rand(1,99),mt_rand(1,29),mt_rand(1,99),mt_rand(1,29),$lineColor);
}


header("Content-Type: image/png");
imagepng($image); // 生成图片
imagedestroy($image); // 销毁一图像，释放资源


?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>页面引用php脚本生成的图片</title>
</head>
<body>
    <img src="create.php" >
</body>
</html>
