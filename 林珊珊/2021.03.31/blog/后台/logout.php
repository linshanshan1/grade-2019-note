<?php
/**
 * 退出登录
 */
session_start();

unset($_SESSION['adminId']);
unset($_SESSION['adminName']);

echo "退出成功!";
echo "<a href='login.php'>返回登录界面</a>";
exit();
