<?php
/**
 * 登录页面
 */
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>登录页面</title>
    <link rel="stylesheet" href="/blog/css/admin.css" type="text/css">
</head>
<body>
<div id="container">
    <h2>非常日记后台管理</h2>
    <div id="middle">
        <p>请输入邮箱和密码</p>
        <form action="login_save.php" method="post">
            <input type="text" name="user_email" class="text" value="邮箱"/>
            <input type="text" name="user_pwd" class="text" value="密码"/>
            <input type="text" name="check" class="check" value="验证码"/>
            <img src="image.php" class="image"/>
            <input type="checkbox" name="rememberMe"/> 记住我
            <input type="submit" value="登录" id="button"/>
        </form>
    </div>
    <a href="#">新员工？创建账号</a>
</div>
</body>
</html>



