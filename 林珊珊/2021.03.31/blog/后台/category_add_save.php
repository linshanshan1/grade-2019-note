<?php
/**
 * 分类增加完成
 */
date_default_timezone_set("PRC");

$addTime = time();
$updateTime = $addTime;

$dsn = "mysql:host=127.0.0.1;dbname=blog";
$db = new PDO($dsn,'root','123456');
$db ->exec("set name utf8mb4");

//分类名称1-10
if(mb_strlen($_POST['category_name']) < 1 || mb_strlen($_POST['category_name']) > 10 ){
    echo '分类名称限制5-10个字';
    echo '<a href="javascript:void(0)" onclick="history.back();">返回上一页</a>';
    exit();
};
//分类描述10-255
if(mb_strlen($_POST['category_desc']) < 10 || mb_strlen($_POST['category_desc']) > 255){
    echo '分类描述限制10-255';
    echo '<a href="javascript:void(0)" onclick="history.back();">返回上一页</a>';
    exit();
}

$sql = "insert into category(category_name,category_desc,update_time,add_time)
          value('".$_POST['category_name']."','".$_POST['category_desc']."','$addTime','$updateTime');";
$result =$db->exec($sql);

if($result){
    echo "增加成功<br />";
    echo "<a href='category_list.php'>返回主页面</a>";
}else{
    echo "增加失败，错误信息：".$db->errorInfo()[2].",请联系管理员：1250205160@qq.com";
}
?>