<?php
/**
 * 分类增加
 */
session_start();
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>分类增加</title>
    <link rel="stylesheet" href="/blog/css/blog.css" type="text/css">
</head>
<body>
<div id="blog-container">
    <div id="blog-top-left">
        <div id="blog-left-title">
            <h2>博客管理系统</h2>
        </div>
    </div>
    <div id="blog-top-right">
        <div id="blog-right-title">
            <h4>欢迎你：<?php echo $_SESSION['adminName']?> <a href="logout.php">退出登录</a></h4>
        </div>
    </div>
    <div id="blog-left">
        <ul>
            <li><a href="category_list.php">分类管理</a></li>
            <li><a href="article_list.php">文章管理</a></li>
            <li><a href="admin_list.php">管理员</a></li>
        </ul>
    </div>
    <div id="blog_add-right">
        <p><a href="#">首页</a> > <a href="category_list.php">分类列表</a> > <a href="article_list.php">文章列表</a> > <a href="admin_list.php">管理员列表</a></p>
        <form action="category_add_save.php" method="post">
            <table>
                <tr>
                    <td class="title">分类名称:</td>
                    <td class="b"><input type="text" name="category_name"></td>
                </tr>
                <tr>
                    <td class="title">分类说明:</td>
                    <td class="b">
                        <textarea style="width: 350px;height: 200px;" name="category_desc"></textarea>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td class="b">
                        <input type="submit" name="submit" class="btn">
                        <input type="reset" name="reset" class="btn">
                    </td>
                </tr>
            </table>
        </form>
    </div>
</div>
</body>
</html>

