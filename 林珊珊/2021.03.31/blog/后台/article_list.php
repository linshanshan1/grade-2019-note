<?php
/**
 * 文章列表
 */
session_start();
if (empty($_SESSION['adminEmail'])) {
    echo "尚未登陆，请先登录<br />";
    echo "<a href='login.php'>前往登录页面</a>";
    exit();
}

date_default_timezone_set("PRC");

$dsn = "mysql:host=127.0.0.1;dbname=blog";
$db = new PDO($dsn,'root','123456');
$db->exec("set name utf8mb4");

$sql = "select * from article order by article_id asc";
$result =$db->query($sql);
$articleList = $result->fetchAll(PDO::FETCH_ASSOC);

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>文章列表</title>
    <link rel="stylesheet" href="/blog/css/blog.css" type="text/css">
    <script src="/blog/js/jquery.js"></script>
</head>
<body>
<div id="blog-container">
    <div id="blog-top-left">
        <div id="blog-left-title">
            <h2>博客管理系统</h2>
        </div>
    </div>
    <div id="blog-top-right">
        <div id="blog-right-title">
            <h4>欢迎你：<?php echo $_SESSION['adminName']?> <a href="logout.php">退出登录</a></h4>
        </div>
    </div>
    <div id="blog-left">
        <ul>
            <li><a href="category_list.php">分类管理</a></li>
            <li><a href="article_list.php">文章管理</a></li>
            <li><a href="admin_list.php">管理员</a></li>
        </ul>
    </div>
    <div id="blog-right">
        <p><a href="#">首页</a> > <a href="category_list.php">分类列表</a> > <a href="article_list.php">文章列表</a> > <a href="admin_list.php">管理员列表</a></p>
        <p>
            <input type="button" value="全选" id="all">
            <a href="#"> 删除选中任务</a>
            <span><a href="article_add.php">增加文章</a></span>
        </p>
        <table>
            <tr>
                <th>文章ID</th>
                <th>文章标题</th>
                <th>所属分类</th>
                <th>文章简介</th>
                <th>文章内容</th>
                <th>发表时间</th>
                <th>修改时间</th>
                <th>操作</th>
            </tr>
            <?php foreach($articleList as $value):?>
                <tr>
                    <td><?php echo $value['article_id']; ?></td>
                    <td><?php echo $value['article_title']; ?></td>
                    <td><?php
                            $sql = "select * from category where category_id = '{$value['category_id']}';";
                            $result =$db->query($sql);
                            $category = $result->fetch(PDO::FETCH_ASSOC);
                              echo $category['category_name'];

                         ?></td>
                    <td><?php echo $value['article_desc']; ?></td>
                    <td><?php echo $value['content']; ?></td>
                    <td><?php echo date('Y-m-d H:i:s',$value['add_time']); ?></td>
                    <td><?php echo date('Y-m-d H:i:s',$value['update_time']); ?></td>
                    <td>
                        <a href="article_edit.php?id=<?php echo $value['article_id']; ?>">编辑</a>
                        <a href="article_detele.php?id=<?php echo $value['article_id']; ?>">删除</a>
                    </td>
                </tr>
            <?php endforeach;?>
        </table>

    </div>
</div>
<script src="/blog/js/main.js"></script>
</body>
</html>

