<?php
/**
 * 登录完成
 */
$rememberMe = $_POST['rememberMe'] ?? " ";

$dsn = "mysql:host=127.0.0.1;dbname=blog";
$db = new PDO($dsn,"root","123456");

$sql = "select * from admin where admin_email = '".$_POST['user_email']."'";
$result = $db->query($sql);
$admin = $result->fetch(PDO::FETCH_ASSOC);

//判断用户是否存在    判断用户名称，用户密码是否正确
if(($admin) && ($admin['admin_pwd'] === $_POST['user_pwd'])){
    session_start();
    $_SESSION["adminEmail"] = $admin['admin_email'];
    $_SESSION["adminName"]= $admin['admin_name'];
    // 记住我一天
    if ( $rememberMe) {
        setcookie('PHPSESSID',session_id(),time()+86400);
    }

    echo '登录成功<br/>';
    echo "<a href='category_list.php'>进入分类列表</a> > <a href='article_list.php'>进入文章列表</a> > <a href='admin_list.php'>进入管理员列表</a>";
}else{
    echo "登录失败,账号或者密码错误！";
    echo "<a href='javascript:void(0)' onclick='history.back();'>返回上一页</a>";
}