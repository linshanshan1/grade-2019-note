<?php
/**
 * 分类删除
 */

$dsn = "mysql:host=127.0.0.1;dbname=blog";
$db = new PDO($dsn,'root','123456');

//删除前检查列表下是否有文章
$sql = "select count(*) as articleNum from article where category_id = '".$_GET['id']."'";
$result = $db->query($sql);
$articleNum = (int)$result->fetch(PDO::FETCH_ASSOC)['articleNum'];
if($articleNum > 0 ){
    echo "列表下有文章，不可删除！若要想删除，请前往文章列表删除相关的文章<br/>";
    echo "<a href='article_list.php'>文章列表</a>";
    exit();
}

$sql = "delete from category where category_id = '".$_GET['id']."'";
$result =$db->exec($sql);

if($result){
    echo "删除成功<br />";
    echo "<a href='category_list.php'>返回主页面</a>";
}else{
    echo "删除失败，错误信息：".$db->errorInfo()[2].",请联系管理员：1250205160@qq.com";
}
?>