<?php
/**
 * 管理员编辑成功
 */
session_start();
date_default_timezone_set("PRC");

$updateTime = time();

$dsn = "mysql:host=127.0.0.1;dbname=blog";
$db = new PDO($dsn,'root','123456');
$db ->exec("set name utf8mb4");

//管理员名称限制1-10个字
if(mb_strlen($_POST['admin_name']) < 1 || mb_strlen($_POST['admin_name']) > 10){
    echo '管理员名称限制1-10个字';
    echo '<a href="javascript:void(0)" onclick="history.back();">返回上一页</a>';
    exit();
}
//管理员密码限制长度1-10
if(mb_strlen($_POST['admin_pwd']) < 1 || mb_strlen($_POST['admin_pwd']) > 10){
    echo '管理员密码限制长度1-10';
    echo '<a href="javascript:void(0)" onclick="history.back();">返回上一页</a>';
    exit();
}
$sql = "update admin set admin_name = '".$_POST['admin_name']."',admin_pwd = '".$_POST['admin_pwd']."', update_time = '$updateTime'
            where admin_id = '".$_POST['admin_id']."';";
$result =$db->exec($sql);

if($result){
    echo "编辑成功<br />";
    echo "<a href='admin_list.php'>返回管理员列表</a>";
}else{
    echo "编辑失败，错误信息：".$db->errorInfo()[2].",请联系管理员：1250205160@qq.com";
}
