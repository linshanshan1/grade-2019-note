<?php
declare (strict_types = 1);

namespace app\index\controller;

use think\facade\Request;
use think\facade\Validate;

class Index
{
    public function index()
    {
        return '您好！这是一个[index]示例应用';
    }
    public function api()
    {
        $num = Request::param('num');
        $validate = Validate::rule([
            'num|数值' => 'require|between:'.PHP_INT_MIN.','.PHP_INT_MAX,

        ]);
        if (!$validate->check(['num'=>$num])){
            $date = [
                'status|状态' => 1,
                'message|错误信息' => $validate->getError(),
                'date' => [],
            ];
            return json($date);
        }
        $abs = abs($num);

        $date = [
            'status|状态' => 0,
            'message|错误信息' => '',
            'date' => [
                '绝对值' => $abs
            ]
        ];
        return json($date);
    }
}
