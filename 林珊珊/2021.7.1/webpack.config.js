const path = require("path");

module.exports = {
	entry: './src/index.js', // 从src/index.js为入口进行打包
	output: { // 输出配置
		path: path.resolve(__dirname, 'dist'), // 输出目录
		filename: 'main.js' // 输出的js文件
	},
	module: {
		rules: [{
				test: /\.css$/i, // 查找所有css后缀的文件。
				use: ['style-loader', 'css-loader'], // style-loader在前
			},
			{
				test: /\.(png|svg|jpg|jpeg|gif)$/i, // 查找图片后缀的文件。
				type: 'asset/resource',
			},
		]
	},
	mode: 'development', // 设置mode，生产环境：production
}